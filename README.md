# My dotfiles

## Requirements
**NOTE**: The following packages are usually available in the repositories of other distributions too.

### Git
```
$ sudo dnf install git 
```

### GNU Stow
```
$ sudo dnf install stow
```

## Installation
1. Checkout the dotfiles repo in your `$HOME` directory using `git`:
    ```
    $ git clone https://gitlab.com/src4026/dotfiles.git
    $ cd dotfiles
    ```

2. Next, use GNU Stow to create the respective symlinks:
    ```
    $ stow .
    ```
    
3. If the target files exist already on your system, you may get a warnning such as
    ```
    WARNING! stowing . would cause conflicts:
        * existing target is neither a link nor a directory: .bashrc
        * existing target is neither a link nor a directory: .config/rofi/config.rasi
    All operations aborted.
    ```
    In this case, make sure that the target files are not required and enter the following command in your terminal:
    ```
    $ stow --adopt .
    ```
